from core.models import Logger
from django.core.mail import send_mail
from forms import celery_app
from datetime import datetime, timedelta
import pytz


@celery_app.task
def send_mail_celery(title, sender, message):
    send_mail(subject=title, message=message, from_email=sender, recipient_list=[sender])
    print("Mail sent!")


@celery_app.task
def check_date():
    now = datetime.now()
    now = pytz.utc.localize(now)
    Logger.objects.filter(time_created__lte=now - timedelta(days=7)).delete()
